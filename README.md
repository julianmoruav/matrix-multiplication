# Matrix Multiplication Hardware Accelerator, a generator in Chisel

This project serves as a simple but complete example on how to use Chisel to develop circuit generators. In particular, this example features:

* Automatic Verilog code generation from the Chisel description.
* A simple CLI interface to use the Chisel generator in the creation of specific Verilog designs with user-defined parameters.
* Automated testing with comparisons against a golden model.
* Automatic VCD file generation for waveform visualization in viewers like GTKWave.
* CI/CD pipelines in the git repository.

In particular, this porject implements a hardware accelerator for matrix multiplication. The Chisel module is capable of computing the multiplication of two matrices with dimensions `MxN` and `NxP`, whose entries are unsigned integer values with `W` bits of width. All four parameters `M`, `N`, `P` and `W` are defined at compilation time with the use of a makefile.

## Installation

This project was developed in Linux Ubuntu 18.04. The following instructions apply to said system.

1. Scala is a very similar to Java, in fact it uses the JVM form the JDK. To develop in Scala install the Java Virtual Machine:

        sudo apt install default-jdk

2. Install the [Scala Build Tool](https://www.scala-sbt.org/). This tool handles all the dependencies and packages required for a Scala project.

        echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
        echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
        curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
        sudo apt-get update
        sudo apt-get install sbt

3. Confirm the installation:

        sbt --version

4. To visualize the waveforms from the testbench simulations, a program like [GTKwave](http://gtkwave.sourceforge.net/) is needed:

        sudo apt install gtkwave

## Generate Verilog Code

The Chisel generator takes four parameters to specify the dimensions `MxN` and `NxP` of the matrices, as well as the bit width `W` of the values in the matrices. The following command will generate the Verilog code for a circuit that multiplies two matrices of dimensions `4x3` and `3x5`, each of them containing `8-bit unsigned integers`.

        make verilog M=4 N=3 P=5 W=8

The generated Verilog file can be found in `generated/MatrixMultiplication.v`.

## Run the Tests and Visualize Waveforms

To test the generator, 14 different unittests have been implemented. The unittests compare the results from the simulation against a golden model. The golden model uses a the matrix multiplication algorithm implemented in [Breeze's linear algebra library for Scala](https://github.com/scalanlp/breeze). The 14 unittests cover the following cases:

* Square matrices.
* Non-square matrices.
* Scalar product (`1x1` matrices).
* Row vector and matrix product.
* Matrix and column vector product.
* Special matrices: null, identity.
* Big matrices: `32x32`. Tested upto to this size because bigger matrices were taking too long to simulate.
* Big numbers: 64-bit unsigned integers.
* Corner case: matrices whose values will produce overflows. 

To run the tests and generate the VCD files for waveforms:

        make tests

The results of the tests will be printed to the standard output. Each of the test cases will generate its own directory where the VCD and Verilog files will be stored. You can see them under `generated/matrixmultiplications.MatrixMultiplicationTests*/`

To visualize a VCD file:

        gtkwave $VCD_FILE

## Future Work

* Parameterize the data types in the matrices so that it is possible to generate Verilog code that works with: unsigned integers, signed integers, floating point. 
* Migrate to Chisel 3.5 and ChiselTest. Currently working with Chisel 3.2 and ChiselIOTesters.
* Setup a Docker container to quickly get a Chisel development environment up and running.


# About Chisel

Chisel is an incredibly useful hardware construction language (HCL) designed to specify highly parameterized circuit generators. It leverages the Objected-Oriented and Functional programming paradigms from the Scala language to ease the design, implementation and simulation of digital circuits. Amongst Chisel most relevant benefits for hardware designers are:

* Increased productivity through modern programming paradigms.
* Easier to maintain designs that describe wider families of circuits. 
* Circuit simulation without any additional tooling.
* A huge collection of managed libraries with data structures and functions that are available to the Scala language.
* Automatic generation of synthesizable Verilog code for FPGAs or ASICs.
* Completely open source, developed and maintained by academia (UC Berkeley) and important companies in the hardware industry (CHIPS Alliance).

Chisel is implemented as a library for Scala that provides the programmer with useful classes, definitions and conventions that are used to describe a hardware graph in a Flexible Intermediate Representation of RTL (FIRRTL). The FIRRTL contains the essential description of the connections and behaviors of the elements in the circuits. It can then be analyzed and transformed by interpreters to generate Verilog code, run simulations, perform power/area/timing estimations, and much more... The possibilities are open for developers to continue creating interpreters that can consume the FIRRTL and generate useful information from it.

## Chisel's Philosophy

Chisel is a relatively new tool that brings a different perspective to hardware design. It can take some time to feel comfortable with it, so I'll just provide a few ideas that can help you grasp Chisel's philosophy while you study it.

1. First Idea:

        Chisel is not here to replace Verilog; it is here to augment it. 

The fact is that Verilog is the *de facto* tool being used for hardware description in the industry. It is a very well established language, with a huge community, hundreds of tools that build upon it, and many more companies that rely on it. Instead of replacing Verilog, Chisel provides a simpler and more productive way of generating Verilog code that can then be integrated to any existing Verilog workflow. A good analogy to the relationship between Chisel and Verilog is the relationship between C code and assembly code. A C compiler will take higher-level C code and generate low-level assembled x86 code; just like this, Chisel provides a higher-level way of describing circuits in Verilog.

2. Second Idea:

        Chisel and Scala are a team.

When you are programming with Chisel you are actually programming in Scala, which means that everything that works with Scala will probably work with Chisel. Since Chisel is a library/package for Scala, in reality all of the Chisel constructs are Scala objects underneath, Scala objects that happen to represent hardware ideas understood by the Chisel compiler. Feel free to use the Chisel constructs as you might need: pass them as arguments, return them from functions, template their types, conditionally create instances of them, arrange them in Scala data structures, parameterize anything you want... Also, take advantage of Scala to implement golden models that can be used for unittesting.

3. Third Idea:

        A powerful circuit generator is the result of a well defined circuit design in Chisel and well thought generation contraints from Scala.  


This third idea goes hand in hand with the second. The key is to leverage Scala's functionality to conditionally create and connect your Chisel hardware constructs. A set of high-level parameters and contraints can be defined and interpreted in a Scala program whose logic controls the instantiation of Chisel hardware constructs. When done properly, a single Chisel module can describe a whole family of circuits with parameterized bit widths, data types, interfaces, operations, etc. The main Scala program can have a user define these parameters and constraints to generate a specific synthesizable Verilog design where all parameters have been evaluated correctly. 

# References and Further Reading

* [Chisel Home Page](https://www.chisel-lang.org/)
* [Chisel Documentation](https://www.chisel-lang.org/chisel3/docs/introduction.html)
* [Chisel Project Template](https://github.com/freechipsproject/chisel-template)
* [Chisel Book](https://github.com/schoeberl/chisel-book)
* [Benefits of Chisel over Classic HDLs](https://stackoverflow.com/questions/53007782/what-benefits-does-chisel-offer-over-classic-hardware-description-languages)
* [Chisel YouTube Channel](https://www.youtube.com/c/Chisel-lang)
* [CHIPS Alliance Home Page](https://chipsalliance.org/)
* [Google's Experience Bulding Coral's Edge TPU in Chisel](https://www.youtube.com/watch?v=x85342Cny8c)
