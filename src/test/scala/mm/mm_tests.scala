/*========================================================================
   Copyright 2022 Julián Morúa Vindas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
=========================================================================*/

package matrixmultiplications 

import breeze.linalg._
import breeze.numerics._
import chisel3._
import chisel3.iotesters.{Driver, PeekPokeTester}
import matrixmultiplications._
import org.scalatest._
import scala.math._
import scala.util._

/*
 * PeekPokeTester for Matrix Multiplication of Integers.
 * It takes as inputs two matrices that are well defined for the matrix multiplication: matA * matB.
 * Uses breeze's linear algebra library as a golden model for validation.
 */
class MMTester (dut: MatrixMultiplication, matA: DenseMatrix[BigInt], matB: DenseMatrix[BigInt], debug: Boolean=false) extends PeekPokeTester (dut) {
    // Confirm matrices dimensions are valid:
    require(matA.cols == matB.rows)

    // Poke the input signals:
    for (row <- 0 until matA.rows) {
        for (col <- 0 until matA.cols) {
            poke(dut.io.mat1(row)(col), matA(row, col))
        }
    }

    for (row <- 0 until matB.rows) {
        for (col <- 0 until matB.cols) {
            poke(dut.io.mat2(row)(col), matB(row, col))
        }
    }
    step(5)

    // Validate the result:
    val goldenModel = matA * matB
    var strMat = ""
    for (row <- 0 until matA.rows) {
        for (col <- 0 until matB.cols) {
            strMat = strMat + peek(dut.io.out(row)(col)) + "\t"
            expect(dut.io.out(row)(col), goldenModel(row, col))
        }
            strMat = strMat + "\n"
    }

    // If needed print the matrices for debugging:
    if (debug) {
        println(s"\nmatrixA:\n$matA")
        println(s"\nmatrixB:\n$matB")
        println(s"\nresult matrix:\n$strMat")
        println(s"\nexpected matrix:\n$goldenModel")
    }
}

/*
 * Scalatest wrapper for Matrix Multiplication Tests. This also generates the VCD files to visualize waveforms in GTKWave.
 */
class MatrixMultiplicationTests extends FlatSpec with Matchers {

    "Standard: 2x2 matrices, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (2, 2, 2, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Standard: 3x3 matrices, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (3, 3, 3, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Standard: 3x5 and 5x2 matrices, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (3, 5, 2, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Standard: 8x4 and 4x9 matrices, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (8, 4, 9, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: scalar multiplication." should "pass" in {
        val (m, n, p, w) = (1, 1, 1, 16)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: vector-matrix product. 1x10 and 10x3, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (1, 10, 3, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: matrix-vector product. 3x10 and 10x1, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (3, 10, 1, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: null matrix A." should "pass" in {
        val (m, n, p, w) = (3, 3, 3, 8)
        val matA = DenseMatrix.zeros[BigInt](m, n)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: null matrix B." should "pass" in {
        val (m, n, p, w) = (3, 3, 3, 8)
        val matA = randomMatrix(m, n, w)
        val matB = DenseMatrix.zeros[BigInt](n, p)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Special: identity matrix." should "pass" in {
        val (m, n, p, w) = (3, 3, 3, 8)
        val matA = randomMatrix(m, n, w)
        val matB = DenseMatrix.eye[BigInt](n)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Corner case: forced overflows. 4x4 matrices, 4-bit integers with value 0xF." should "pass" in {
        val (m, n, p, w) = (4, 4, 4, 4)
        val matA = DenseMatrix.tabulate(m, n){case (i, j) => BigInt(0xF)}
        val matB = DenseMatrix.tabulate(n, p){case (i, j) => BigInt(0xF)}
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Big numbers: 4x4 matrices, 64-bit random integers." should "pass" in {
        val (m, n, p, w) = (8, 8, 8, 64)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Big matrices: 32x32 matrices, 8-bit random integers." should "pass" in {
        val (m, n, p, w) = (32, 32, 32, 8)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    "Big numbers and big matrices: 24x24 matrices, 64-bit random integers." should "pass" in {
        val (m, n, p, w) = (24, 24, 24, 64)
        val matA = randomMatrix(m, n, w)
        val matB = randomMatrix(n, p, w)
        val testResult = chisel3.iotesters.Driver.execute(Array("--generate-vcd-output", "on", "--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w)){c => new MMTester(c, matA, matB)}
        assert(testResult)
    }

    // Helper function to generate matrices with random values. The max value is 2^w, where w is the width of the buses.
    def randomMatrix(rows: Int, cols: Int, w: Int) = {
        val randgen = Random
        val maxRand = math.pow(2, w)
        val mat = DenseMatrix.tabulate(rows, cols){case (i, j) => BigInt(randgen.nextInt(maxRand.toInt))}
        mat // return
    }
}
